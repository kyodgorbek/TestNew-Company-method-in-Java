# TestNew-Company-method-in-Java




public class TestNewCompany {
 
 public static void main(String[] args){
  // Create a Company object with 3 elements
  NewCompany myCompany = new NewCompany(3);
  
  // Store 3 employees of different kinds in the array
  myCompany.setEmployee( new HourlyEmployee("Sam",  "1234", "clerk", "", "", 30, 2000, 15000.00, 40.0, 15.0), 0);
  myCompany.setEmployee( new SalaryEmployee("Jessica", "2222", "manager", "", "", 33, 1998, 40000.00, 52000.00), 1);
  
  myCompany.setEmployee(new NewEmployee("George", "3456", "spouse of boss", "", "", 55, 1990, 0.0), 2);
  
  //Update total pay for each employee.
  myCompany.computePayroll();
  
  //Display employee information
  System.out.println(myCompany.toString());
 }
}  
